<?php
define('EXT', '.php');
define('PATH_ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR);

spl_autoload_register('autoload');

function autoload($class)
{
    $parts = explode('_', $class);
    $path = PATH_ROOT . implode(DIRECTORY_SEPARATOR, $parts);
    if (is_readable($path . EXT)) {
        require_once $path . EXT;
    }
}